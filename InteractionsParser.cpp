#include "InteractionsParser.h"
#include "StringTokenizer.h"

#include <algorithm>
#include <iostream>
#include <stdexcept>

InteractionsParser::InteractionsParser(const char *ini_filename) :
	_ini(false, true, false) {
	SI_Error rc = _ini.LoadFile(ini_filename);
	if (rc < 0)
		throw runtime_error("Failed loading interaction ini file: "
				+ CSimpleIni::str_SI_Error(rc));

	const CSimpleIni::TKeyVal *section = _ini.GetSection("Interactions");
	if (!section)
		throw runtime_error("Interactions ini file is missing "
			"'Interactions' section.");

	CSimpleIni::TNamesDepend interactionTypes;
	_ini.GetAllKeys("Interactions", interactionTypes);

	for (CSimpleIni::TNamesDepend::const_iterator key =
			interactionTypes.begin(); key != interactionTypes.end(); ++key) {

		CSimpleIni::TNamesDepend interactions;
		_ini.GetAllValues("Interactions", key->pItem, interactions);

		for (CSimpleIni::TNamesDepend::const_iterator value =
				interactions.begin(); value != interactions.end(); ++value) {
			parseInteraction(key, value);

		}
	}
}

void InteractionsParser::parseInteraction(
		CSimpleIni::TNamesDepend::const_iterator key,
		CSimpleIni::TNamesDepend::const_iterator value) {
	StringTokenizer strtok = StringTokenizer(value->pItem, ",");
	int cnt = strtok.countTokens();
	// cout << "cnt is " << cnt << endl;
	if (cnt == 0)
		throw runtime_error("Empty \"" + string(key->pItem) + "\" key.");
	Interaction interaction;
	interaction.name = "";

	for (int i = 0; i < cnt; i++) {
		double x_frac, y_frac;
		double energyValue = 0.0;
		string tmp = strtok.nextToken();
		cout << "tmp is: " << tmp << endl;
		string::size_type loc = tmp.find( "/", 0 );
		if (sscanf(tmp.c_str(), "%lf/%lf", &x_frac, &y_frac) != 2) // || x_frac < 0	|| y_frac < 0) // interactions may have negative vectors!
			{
			if (sscanf(tmp.c_str(), "%lf", &energyValue) == 1 && i == cnt - 2)
				interaction.energy = energyValue;
			// Treat last tuple as comment
			else if (i == cnt -1 ) {
				interaction.name = tmp;
				break;
			}
			else
				throw runtime_error("Skrewed interaction definition in: "
						+ string(tmp));
		};
		if (i != cnt and (loc != string::npos)) {
			 cout << "pushing x: " << x_frac << " and y: " << y_frac << endl;
			interaction.directions.push_back(Direction(x_frac, y_frac));
		};
	}
	// TODO: check cardinality (trio, etc...)
	/*
	cout << "read the following values: " << endl;
	cout << "name: " << interaction.name << endl;
	cout << "directions: "<< endl;
	int counter = 0;
	for (Directions::iterator i = interaction.directions.begin(); i != interaction.directions.end(); i++){
		cout << counter << "th direction has x= " << i->x << " and y=" << i->y << endl;
		counter++;
	};
	*/
	_interactions.push_back(interaction);
}

Interactions InteractionsParser::getInteractions() {
	// TODO: auto_ptr

	/* DEBUG
	cout << "before sorting " << endl;
	for (Interactions::iterator i = _interactions.begin(); i != _interactions.end(); i++){
			cout << i->name << endl;
	};
	*/
	// automatically sorting interactions lexicographically to given name from interactions.ini
	sort(_interactions.begin(), _interactions.end(), Interactions::compareInteraction);
	/*
	cout << "after sorting " << endl;
	for (Interactions::iterator i = _interactions.begin(); i != _interactions.end(); i++){
		cout << i->name << endl;
	};
	*/

	return _interactions;
}

