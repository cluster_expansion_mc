################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ConvertUTF.c 

CPP_SRCS += \
../Interactions.cpp \
../InteractionsParser.cpp \
../Lattice.cpp \
../LatticeParser.cpp \
../MonteCarlo.cpp \
../StringTokenizer.cpp \
../Structure.cpp \
../StructureParser.cpp \
../mc_main.cpp 

OBJS += \
./ConvertUTF.o \
./Interactions.o \
./InteractionsParser.o \
./Lattice.o \
./LatticeParser.o \
./MonteCarlo.o \
./StringTokenizer.o \
./Structure.o \
./StructureParser.o \
./mc_main.o 

C_DEPS += \
./ConvertUTF.d 

CPP_DEPS += \
./Interactions.d \
./InteractionsParser.d \
./Lattice.d \
./LatticeParser.d \
./MonteCarlo.d \
./StringTokenizer.d \
./Structure.d \
./StructureParser.d \
./mc_main.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


