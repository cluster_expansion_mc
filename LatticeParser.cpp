#include <stdexcept>

#include "LatticeParser.h"

LatticeParser::LatticeParser(const char *ini_file) :
	_ini(false, true, false) {
	SI_Error rc = _ini.LoadFile(ini_file);
	if (rc < 0)
		throw runtime_error("Failed loading structure ini file: "
				+ CSimpleIni::str_SI_Error(rc));

	// Parse UnitCell
	const CSimpleIni::TKeyVal *section = _ini.GetSection("UnitCell");
	if (!section)
		throw runtime_error("Ini file is missing 'UnitCell' section.");

	const char *tmp;
#define ASSIGN_KEY(key_name, name) \
	tmp = _ini.GetValue("UnitCell", key_name); \
	if (!tmp) \
	throw runtime_error("UnitCell definition is missing " \
			key_name); \
	int name = atoi(tmp); \

	ASSIGN_KEY("UnitCellSizeX", UnitCellSizeX);
	ASSIGN_KEY("UnitCellSizeY", UnitCellSizeY);
	ASSIGN_KEY("GranularityX", GranularityX);
	ASSIGN_KEY("GranularityY", GranularityY);
#undef ASSIGN_KEY

	if (UnitCellSizeX < 1 || UnitCellSizeY < 1 || GranularityX < 1
			|| GranularityY < 1)
		throw runtime_error("Unit cell has unreasonable size.");

	// increasing Granularity to get proper structure representation
	// GranularityX = GranularityX + 1;
	// GranularityY = GranularityY + 1;

	_lattice
			= new Lattice(UnitCellSizeX, UnitCellSizeY, GranularityX, GranularityY);
	_lattice->surface = empty;
	_lattice->adsorbates = empty;

	section = _ini.GetSection("Surface");
	if (!section)
		throw runtime_error("Ini file is missing 'Surface' section. ");

	CSimpleIni::TNamesDepend Pd_positions;
	_ini.GetAllValues("Surface", "Pd", Pd_positions);
	for (CSimpleIni::TNamesDepend::const_iterator i = Pd_positions.begin(); i
			!= Pd_positions.end(); ++i) {
		double x_frac, y_frac;
		if (sscanf(i->pItem, "%lf/%lf", &x_frac, &y_frac) != 2 || x_frac < 0
				|| x_frac >= 1 || y_frac < 0 || y_frac >= 1)
			throw runtime_error("Skrewed coordinates in " + string(i->pItem));
		int inter_x, inter_y;
		inter_x = _lattice->UnitCellSizeX * _lattice->GranularityX;
		inter_y = _lattice->UnitCellSizeY * _lattice->GranularityY;
		double double_x, double_y;
		double_x = inter_x * x_frac;
		double_y = inter_y * y_frac;
		int pos_x, pos_y;
		pos_x = round(double_x);
		pos_y = round(double_y);
		cout << "filling (" << pos_x << "/" << pos_y << ") with Pd" << endl;
		_lattice->surface(pos_x, pos_y) = Pd;
	}

	// Parse Adsorbates
	section = _ini.GetSection("Adsorbates");
	if (!section)
		throw runtime_error("Ini file is missing 'Adsorbates' section. ");

	CSimpleIni::TNamesDepend CO_positions;
	_ini.GetAllValues("Adsorbates", "CO", CO_positions);
	_lattice->nrCO = CO_positions.size();
	for (CSimpleIni::TNamesDepend::const_iterator i = CO_positions.begin(); i
			!= CO_positions.end(); ++i) {
		double x_frac, y_frac;
		if (sscanf(i->pItem, "%lf/%lf", &x_frac, &y_frac) != 2 || x_frac < 0
				|| x_frac >= 1 || y_frac < 0 || y_frac >= 1)
			throw runtime_error("Skrewed coordinates in " + string(i->pItem));
		int inter_x, inter_y;
		inter_x = _lattice->UnitCellSizeX * _lattice->GranularityX;
		inter_y = _lattice->UnitCellSizeY * _lattice->GranularityY;
		double double_x, double_y;
		double_x = inter_x * x_frac;
		double_y = inter_y * y_frac;
		int pos_x, pos_y;
		pos_x = round(double_x);
		pos_y = round(double_y);
		cout << "filling (" << pos_x << "/" << pos_y << ") with CO" << endl;
		_lattice->adsorbates(pos_x, pos_y) = CO;
	}

	// Parse Symmetry options
	/*
	CSimpleIni::TNamesDepend Symmetry_Operations;
	_ini.GetAllValues("Symmetry", "Sym_Operation", Symmetry_Operations);
	for (CSimpleIni::TNamesDepend::const_iterator i =
			Symmetry_Operations.begin(); i != Symmetry_Operations.end(); ++i) {
		double x_frac, y_frac;
		if (sscanf(i->pItem, "%lf/%lf", &x_frac, &y_frac) != 2)
			throw runtime_error("Skrewed coordinates in " + string(i->pItem));
		_lattice->_symmetryOperations.directions.push_back(Direction(x_frac,
				y_frac));
	}
	*/
}

Lattice *LatticeParser::getLattice() {
	// FIXME: auto_ptr?
	return _lattice;
}
