#ifndef _LATTICEPARSER_H
#define _LATTICEPARSER_H

#include "Lattice.h"
#include "SimpleIni.h"

class LatticeParser {
public:
	LatticeParser(const char *ini_filename);
	Lattice* getLattice();

private:
	Lattice* _lattice;
	CSimpleIni _ini;
};

#endif

