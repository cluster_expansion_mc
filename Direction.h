#ifndef DIRECTION_H_
#define DIRECTION_H_




struct Direction {
	double x;
	double y;
	Direction(double x, double y) : x(x), y(y) {};
};

typedef vector<Direction> Directions;




#endif /*DIRECTION_H_*/
