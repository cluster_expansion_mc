#include <stdexcept>

#include "Lattice.h"
#include "SimpleIni.h"

// FIXME: LatticeType should derive operator= from Array<Occupation, 2>
LatticeLayer& LatticeLayer::operator=(Occupation const& o) {
	for (LatticeLayer::iterator i = this->begin(); i != this->end(); ++i)
	this->operator()(i.position()) = o;
	return *this;
};

ostream& operator<<(ostream& output, const LatticeLayer& layer) {
	for (int y = 0; y < layer.extent(secondDim); ++y) {
		for (int x = 0; x < layer.extent(firstDim); ++x) {
			switch (layer(x, y)) {
			case empty:
				output << ".";
				break;
			case CO:
				output << "C";
				break;
			case Pd:
				output << "P";
				break;
			default:
				throw out_of_range("Invalid occupation of Lattice Layer");
			}
		}
		output << endl;
	}

	return output;
}

ostream& operator<<(ostream& output, const Lattice& lattice) {
#define OUTPUT(l) \
	for (int y = 0; y < l.extent(secondDim); ++y) { \
		if (y && y % lattice.GranularityY == 0) { \
			for (int i = 0; i < lattice.UnitCellSizeX * \
					(lattice.GranularityX+1)-1; ++i) { \
				if (i && (i+1) % (lattice.GranularityX+1) == 0) output << "+"; \
				else output << "-"; \
			} \
			output << endl; \
		} \
		for (int x = 0; x < l.extent(firstDim); ++x) { \
			if (x && x % lattice.GranularityX == 0) output << "|"; \
			switch (l(x,y)) { \
			case empty: output << "."; break; \
			case CO:	output << "C"; break; \
			case Pd:	output << "P"; break; \
			default: 	throw out_of_range("Invalid occupation"); \
			} \
		} \
		output << endl; \
	}

	output << "lattice: UnitCellSizeX = " << lattice.UnitCellSizeX
			<< ", UnitCellSizeY = " << lattice.UnitCellSizeY
			<< ", GranularityX = " << lattice.GranularityX
			<< ", GranularityY = " << lattice.GranularityY << endl << endl
			<< "surface: " << endl;
	OUTPUT(lattice.surface);
	output << endl << "adsorbates: " << endl;
	OUTPUT(lattice.adsorbates);
	output << endl;

	return output;
}



Lattice::Lattice(int UnitCellSizeX, int UnitCellSizeY, int GranularityX,
		int GranularityY) :
	GranularityX(GranularityX), GranularityY(GranularityY), UnitCellSizeX(
			UnitCellSizeX), UnitCellSizeY(UnitCellSizeY) {
	TinyVector<int, 2> extent(UnitCellSizeX * GranularityX, UnitCellSizeY
			* GranularityY);
	adsorbates.resize(extent);
	surface.resize(extent);
}




void Lattice::assessInteractions(Interactions &interactions) {
	for (LatticeLayer::iterator i = adsorbates.begin(); i != adsorbates.end(); i++) {
		if (adsorbates(i.position()) == empty)
			continue;
		//cout << "checking interactions at POS " << i.position() << endl; // << "occupation is: " << adsorbates(i.position()) << endl;
		for (Interactions::iterator interaction = interactions.begin(); interaction
				!= interactions.end(); interaction++) {
			//cout << "checking " << interaction->name << ": " << endl;
			//		<< " with " << interaction->directions.size()
			//		<< " vectors." << endl;

			// resetting multiplicity initially
			interaction->multiplicity = 0;

			int x0 = (i.position()[firstDim]);
			int y0 = (i.position()[secondDim]);

			int pos_x, pos_y;
			pos_x = 0;
			pos_y = 0;

			// counting number of hits with interaction Vectors
			int hitCounter = 0;

			// checking directions
			for (Directions::iterator direction =
					interaction->directions.begin(); direction
					!= interaction->directions.end(); direction++) {

				//cout << "checking direction vector: (" << direction->x << "/" << direction->y << ")" << endl;

				pos_x = x0 + direction->x * GranularityX;
				pos_y = y0 + direction->y * GranularityY;

				pos_x = (int) (pos_x) % (int) (UnitCellSizeX * GranularityX);
				pos_y = (int) (pos_y) % (int) (UnitCellSizeY * GranularityY);

				while (pos_x < 0) {
					pos_x += (UnitCellSizeX * GranularityX);
				};
				while (pos_y < 0) {
					pos_y += (UnitCellSizeY * GranularityY);
				};

				//cout << "checking POS: (" << pos_x << "/" << pos_y << ")"
				//		<< endl;
				if (adsorbates(pos_x, pos_y) != empty)
					hitCounter++;
			};

			if (hitCounter == (int) interaction->directions.size()) {
				//cout << "HIT " << endl;
				(interaction->multiplicity)++;
			};

			// reset hitcounter, pos_x, pos_y
			hitCounter = 0;
			pos_x = 0;
			pos_y = 0;

			// checking symmetry directions
			for (vector<Directions>::iterator symmetryVec =
					interaction->symmetryDirections.begin(); symmetryVec
					!= interaction->symmetryDirections.end(); symmetryVec++) {
				// reset hitcounter, pos_x, pos_y
				hitCounter = 0;
				pos_x = 0;
				pos_y = 0;

				for (Directions::iterator symmetryDirection =
						symmetryVec->begin(); symmetryDirection
						!= symmetryVec->end(); symmetryDirection++) {

					//cout << "checking symmetry vector: (" << symmetryDirection->x << "/" << symmetryDirection->y << ")" << endl;

					pos_x = x0 + symmetryDirection->x * GranularityX;
					pos_y = y0 + symmetryDirection->y * GranularityY;

					pos_x = (int) (pos_x)
							% (int) (UnitCellSizeX * GranularityX);
					pos_y = (int) (pos_y)
							% (int) (UnitCellSizeY * GranularityY);

					while (pos_x < 0) {
						pos_x += (UnitCellSizeX * GranularityX);
					};
					while (pos_y < 0) {
						pos_y += (UnitCellSizeY * GranularityY);
					};

					//cout << "checking POS: (" << pos_x << "/" << pos_y << ")"
					//		<< endl;
					// place to implement several atom species checking!!
					if (adsorbates(pos_x, pos_y) != empty)
						hitCounter++;
				};

				if (hitCounter == (int) symmetryVec->size()) {
					//cout << "HIT " << endl;
					(interaction->multiplicity)++;
				};

			};
		};
		//cout << "next adsorbate..." << endl << endl;
	};
	//cout << "all interactions checked..." << endl << endl;
	return;
}


