#ifndef STRUCTURE_H_
#define STRUCTURE_H_

#include <blitz/array.h>
using namespace blitz;

#include <map>

#include "Lattice.h"
#include "Interactions.h"

using namespace std;


/* newmat stuff - maybe we need this for order parameter fft, but not now
#define WANT_STREAM                  		// include.h will get stream fns
#define WANT_MATH                    		// include.h will get math fns
// newmatap.h will get include.h

#include "newmat/newmatap.h"                // need matrix applications
#include "newmat/newmatio.h"                // need matrix output routines
#ifdef use_namespace
using namespace NEWMAT; // access NEWMAT namespace
#endif
*/



class Structure {
public:

	string structureName;
	Lattice *lattice;
	Interactions interactions;
	// to be read in from structure ini file... if not present -> new structure!!
	double dftEnergy;
	double CEenergy;
	bool newStructure;
	double onSiteEnergy;
	// vector<double> onSiteEnergies; better use a std::map like onSiteEnergies <species,energy>


	Structure() :
		structureName(""), lattice(), interactions(), dftEnergy(0), newStructure(false) {
	};

	Structure(Interactions interactions) :
		structureName(""), lattice(), interactions(interactions), dftEnergy(0), newStructure(false) {

	};

	double getEnergy(void);



};

class Structures: public vector<Structure *> {
public:
	// Structures();
	// Structures(Structure);
	// Interaction(const string name = "", int multiplicity = 0, double energy = 0.0) :
	// 	name(name), multiplicity(multiplicity), energy(energy) {};
	// anything needed here?

	// methode: new_structure()
	// und dann je nach parameter: random.. oder alles an CO positionen durchprobieren etc..
	// oder vielleicht sogar per input file einlesen...

	//double cvScoreValueLOOCV;
	//double cvScoreValueMCCV;

	//Structures restrictInteractions(Structures& structures);
	void printEnergies(Structures& structures);
	//void fitInteractionEnergies(Structures& structures);
	//void fitInteractionEnergies(Structures& structures, map<string, double>& interactionEnergies);


	//double calcMCCV(Structures& structures); // does expansion for Monte Carlo Cross Validation
	//double calcLOOCV(Structures& structures); // does expansion for Leave One Out Cross Validation
											  // and fits energies by fitInteractionEnergies

};

/*
// move into appropriate subroutines!
typedef Structures ValidationSet, FitSet;
typedef vector<Structures> ValidationSets, FitSets;
*/
#endif /*STRUCTURE_H_*/
