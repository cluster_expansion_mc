#include <stdexcept>

#include <blitz/array.h>
#include <random/uniform.h> 				// blitz RNG Mersenne Twister
#include <set>
using namespace std;
using namespace blitz;
using namespace ranlib;

#include "Lattice.h"
#include "Interactions.h"
#include "Structure.h"

/*
#define WANT_STREAM                  		// include.h will get stream fns
//#define WANT_MATH                    		// include.h will get math fns
#define USING_DOUBLE						// defines newmat with double precision
											// - we can maybe use this tyedefinition of Real type in newmat to get even more precision with libgmp.a
// newmatap.h will get include.h

#include "newmat/newmatap.h"                // need matrix applications
#include "newmat/newmatio.h"                // need matrix output routines
#ifdef use_namespace
using namespace NEWMAT; // access NEWMAT namespace
#endif
*/



void Structures::printEnergies(Structures& structures) {
	int counter = 1;
	double meanSquareDifference = 0.0;

	for (Structures::iterator structure = structures.begin(); structure != structures.end(); structure++){
		double deltaE = ((*structure)->dftEnergy - (*structure)->onSiteEnergy);
		cout << "Structure Nr: " << counter << " got: \n" <<
		"\t CE  energy of: " << (*structure)->getEnergy() << "\n" <<
		"\t - DFT energy of: " << (*structure)->dftEnergy << "\n" <<
		"\t onSite energy: " << (*structure)->onSiteEnergy << "\n" <<
		"\t DFT - onSite: " << deltaE << "\n" <<
		"\t delta(CE - DFT): " <<  ((*structure)->getEnergy() - deltaE)   << "\n";
		meanSquareDifference += ((*structure)->getEnergy() - deltaE) * ((*structure)->getEnergy() - deltaE);
		cout << " meanSquareDifference: " << meanSquareDifference << "\n";
		counter++;
	};

	double CVScore = sqrt( meanSquareDifference / ((double) counter));

	cout << " finally the CV Score for this: " << CVScore << "\n";

	cout << " now showing the power of combinations.hpp: \n";

	Structures::iterator referenceStructure = structures.begin();

	// just a test to see, if i can call the combinations routine here
	//(*referenceStructure)->interactions.generateCombinations();

	return;
}


double Structure::getEnergy() {
	// FIXME: check adsorbate species and get correct on-site energy contribution

	// get interaction multiplicities for current configuration
	// lattice->assessInteractions(interactions);

	double energy = 0.0;
	// energy += onSiteEnergy; // TODO : adsorbate_on_site_energy(species and site specific!)
							// so far only sum value for all species present solved in input file
	/* no need for this yet, since onSiteEnergy gives sum value
	for (LatticeLayer::iterator i = lattice->adsorbates.begin(); i
			!= lattice->adsorbates.end(); i++) {
		if (lattice->adsorbates(i.position()) == empty)
			continue;
		// implement species checking CO or O or whatever
		};
	*/
	for (Interactions::iterator interaction = interactions.begin(); interaction
			!= interactions.end(); interaction++) {
		// cout << "calculating energy contribution of " << endl
		//	<< interaction->name << " with " << interaction->multiplicity << "*" << interaction->energy << endl;
		int interactionMultiplier = 0; // to take into account that we fitted all interaction energies normalized to 1 atom
		switch (interaction->directions.size()) {
			case (1) : { interactionMultiplier = 2; break; }; // pair interaction
			case (2) : { interactionMultiplier = 3; break; }; // trio interaction
			case (3) : { interactionMultiplier = 4; break; }; // quattro interaction
			case (4) : { interactionMultiplier = 5; break; }; // quinto interaction
		};
		energy += (interaction->energy * interaction->multiplicity); // * interactionMultiplier);
	};

	//energy *= lattice->nrCO;
	energy += onSiteEnergy;
			// cout << "total structure energy is: " << energy << endl;
	//energy = energy * (-1); // changing sign of energy - TODO: why is that necessary?
	return energy;
}


