/*
 * MonteCarlo.h
 *
 *  Created on: Feb 5, 2010
 *      Author: rieger
 */

#ifndef MONTECARLO_H_
#define MONTECARLO_H_

#include "Structure.h"
#include "Interactions.h"


class MonteCarlo {
	long mcSteps;
	double temperature;
	static const double kBoltzmann = 8.617386e-05;// in eV*K^-1
	Structure mcStructure;
	void mcMove();
	void mcSnapshot();
};


#endif /* MONTECARLO_H_ */
