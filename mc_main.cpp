/*
 * Canonical Monte Carlo Program
 * based on cluster_expansion.cpp
 * Monte Carlo Sampling of CO adsorption structures on a square Pd(100) surface
 * including the effects of lateral interactions by cluster expansion method
 *
 * Michael Rieger, FHI, 2010
 * rieger@fhi-berlin.mpg.de
 *
 */

#include <iostream>
#include <fstream>
#include <memory>
#include <algorithm>

#include "Lattice.h"
#include "LatticeParser.h"

#include "Interactions.h"
#include "InteractionsParser.h"

#include "Structure.h"
#include "StructureParser.h"

void usage(const char *prog) {
	cerr << "usage: " << prog
			<< " interactions-ini-file structure-ini-file monteCarlo-run-parameter-file ... " << endl;

	exit(EXIT_FAILURE);
}

int main(int argc, char* argv[]) {
	if (argc < 4)
		usage(*argv);

	try {

		Structures structures;

		cout<< endl << "#################### READING INPUT ################ " << endl;
		// argv[1] = interactions file , argv[2] = initial structure, argv[3] = mc run parameters
		StructureParser structureParser(argv[1], argv[2]);
		Structure *structure = structureParser.getStructure();
		structures.push_back(structure);
		cout << ".. done parsing structure and interactions" << endl;

		cout<< endl << "#################### SETTING UP MONTE CARLO ################ " << endl;
		cout << "the git works for me - tralalala\n";



		//structures.printEnergies(structures);






	} catch (exception& e) {
		cerr << "FATAL: " << e.what() << endl;
		exit(EXIT_FAILURE);
	}

	exit(EXIT_SUCCESS);
}
