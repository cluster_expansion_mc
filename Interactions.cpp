#include <stdexcept>
#include <string>
#include <iostream>


#include "Interactions.h"
#include "SimpleIni.h"
#include "Combination.hpp" // template class to generate all combinations of a vector - we need this for the interactions

using namespace std;
using namespace boost; // needed for using the functions of combinations.hpp
using namespace detail; // see above

ostream& operator<<(ostream& output, const Interactions& interactions) {
	for (Interactions::const_iterator interaction = interactions.begin(); interaction
			!= interactions.end(); interaction++) {
		output << (interaction->name.empty() ? "(no name)" : interaction->name)
				<< " [M:" << interaction->multiplicity << ", E:"
				<< interaction->energy << "]\t\t: ";
		for (Directions::const_iterator direction =
				interaction->directions.begin(); direction
				!= interaction->directions.end(); direction++) {
			output << "(" << direction->x << "/" << direction->y << ") ";
		}
		output << endl;
		for (vector<Directions>::const_iterator directions =
				interaction->symmetryDirections.begin(); directions
				!= interaction->symmetryDirections.end(); directions++) {
			output << "\t\t\t";
			for (Directions::const_iterator direction = directions->begin(); direction
					!= directions->end(); direction++) {
				output << "(" << direction->x << "/" << direction->y
						<< ") ";
			}
			output << endl;
		}
		output << endl;
	}
	return output;
}


ostream& operator<<(ostream& output, const Interactions::iterator interaction) {
	output << (interaction->name.empty() ? "(no name)" : interaction->name)
					<< " [M:" << interaction->multiplicity << ", E:"
					<< interaction->energy << "]\n";
	output << endl;
	return output;
}


bool Interactions::compareInteraction(const Interaction a, const Interaction b) {
	return (a.name < b.name);
}



bool operator==(Directions& firstDirections, Directions& secondDirections) {

	/*
	 * Defining equality operator for Directions:
	 * A direction is equal to another one, if it contains the same vectors (direction.x/direction.y),
	 * even if they are arranged in a different order in this direction
	*/
	if (firstDirections.size() != secondDirections.size()) {
		cout << "Directions to compare got different number of vectors" << endl;
		return false;
	}

	bool equalVector;
	int counterEqualVectors = 0;

	for (Directions::iterator firstDirection = firstDirections.begin(); firstDirection
			!= firstDirections.end(); firstDirection++) {

		for (Directions::iterator secondDirection = secondDirections.begin(); secondDirection
				!= secondDirections.end(); secondDirection++) {
			equalVector = false;
			//cout << "comparing vector 1st direction:(" << firstDirection->x << "/" << firstDirection->y << ") with vector 2nd direction:(" << secondDirection->x << "/" << secondDirection->y << ") " << endl;
			if (firstDirection->x == secondDirection->x && firstDirection->y
					== secondDirection->y) {
				equalVector = true;
			}
			if (equalVector) {
				counterEqualVectors++;
				break;
			};
		}
	}

	if (counterEqualVectors == (int) firstDirections.size()) {
		//cout << "Directions equal!" << endl;
		return true;
	}
	else
		return false;

}


Direction Interaction::mirrorXZ(Directions::iterator direction) {
	double newX, newY;
	newX = direction->x;
	newY = - direction->y;
	Direction newDirection(newX, newY);

	return newDirection;
}

Direction Interaction::mirrorYZ(Directions::iterator direction) {
	double newX, newY;
	newX = - direction->x;
	newY = direction->y;
	Direction newDirection(newX, newY);

	return newDirection;
}

vector<Directions> Interaction::rotate90degreeZ(Directions& directions) {

	//cout << "Rotations..." << endl;

	Directions newDirections;
	vector<Directions> returnDirectionsVector;

	returnDirectionsVector.push_back(directions);

	for (int nrOfRotations = 0; nrOfRotations < 4; nrOfRotations++){

		vector<Directions>::iterator lastRotatedDirections;
		lastRotatedDirections = returnDirectionsVector.end() - 1;

		for (Directions::iterator direction = lastRotatedDirections->begin(); direction != lastRotatedDirections->end(); direction++){
			double newX, newY;
			newX = - direction->y;
			newY = direction->x;
			Direction newDirection(newX, newY);
			newDirections.push_back(newDirection);
		};
		returnDirectionsVector.push_back(newDirections);
		newDirections.clear();
	}



	return returnDirectionsVector;

}

void Interaction::generateSymmetryEquivalentVecC2v(Directions directions,
		vector<Directions>& symmetryDirections) {

	Directions newDirections;
	for (Directions::iterator direction = directions.begin(); direction
			!= directions.end(); direction++) {
		Direction tempDirection(mirrorXZ(direction));
		//cout << "mirrorXZ yields for: (" << direction->x << "/" << direction->y
		//		<< ") the new: (" << tempDirection.x << "/" << tempDirection.y
		//		<< ") " << endl;
		newDirections.push_back(tempDirection);
	}
	symmetryDirections.push_back(newDirections);
	newDirections.clear();

	for (Directions::iterator direction = directions.begin(); direction
			!= directions.end(); direction++) {
		Direction tempDirection(mirrorYZ(direction));
		//cout << "mirrorYZ yields for: (" << direction->x << "/" << direction->y
		//		<< ") the new: (" << tempDirection.x << "/" << tempDirection.y
		//		<< ") " << endl;
		newDirections.push_back(tempDirection);
	}
	symmetryDirections.push_back(newDirections);
	newDirections.clear();

	vector<Directions> newSymmetryDirections;

	// Rotating of Symmetry Directions
	for (vector<Directions>::iterator symmetryDirection =
			symmetryDirections.begin(); symmetryDirection
			!= symmetryDirections.end(); symmetryDirection++) {
			vector<Directions> rotatedDirections;
			rotatedDirections = rotate90degreeZ((*symmetryDirection));
			newSymmetryDirections.insert(newSymmetryDirections.end(), rotatedDirections.begin(), rotatedDirections.end());
			rotatedDirections.clear();
	}

	// Now rotating the original set of directions finally
	vector<Directions> rotatedDirections;
	rotatedDirections = rotate90degreeZ(directions);
	newSymmetryDirections.insert(newSymmetryDirections.end(), rotatedDirections.begin(), rotatedDirections.end());

	// newSymmetryDirections.push_back(rotate90degreeZ(directions));

	// Joining newly generated newSymmetryDirections and symmetryDirections
	symmetryDirections.insert(symmetryDirections.end(), newSymmetryDirections.begin(), newSymmetryDirections.end());


	rotatedDirections.clear();
	newSymmetryDirections.clear();

	/* DEBUG output
	cout << " -------- complete symmetryDirections before checking doubles----- " << endl;
	for (vector<Directions>::iterator symmetryVec1 = symmetryDirections.begin(); symmetryVec1
				!= symmetryDirections.end(); symmetryVec1++) {
		//cout << "new interaction" << " with " << symmetryVec1->size() << " as size" << endl;
		for (Directions::iterator direction = symmetryVec1->begin(); direction != symmetryVec1->end(); direction++){
			//cout << ". - vector (" << direction->x << "/" << direction->y << ")" << endl;
		}
	}

	cout << " -------- complete symmetryDirections  end ------------------------ " << endl << endl;
	*/
	// sorting out double occurrences of interaction vectors
	//cout << "sorting out double occurencies!" << endl << endl << endl;


	//cout << symmetryDirections.size() << " symmetrized Directions after rotations and mirroring" << endl;


	vector<Directions> uniqueSymmetryDirections;

	for (vector<Directions>::iterator symmetryDirections1 =
			symmetryDirections.begin(); symmetryDirections1
			!= symmetryDirections.end(); symmetryDirections1++) {
		//cout << "comparing to original directions" << endl;
		if (*symmetryDirections1 == directions) {
			continue;
			};
		if (uniqueSymmetryDirections.empty())
			uniqueSymmetryDirections.push_back(*symmetryDirections1);

		vector<Directions> comparisonDirections;
		comparisonDirections.insert(comparisonDirections.end(), uniqueSymmetryDirections.begin(), uniqueSymmetryDirections.end());
		bool alreadyPresentInteraction = false;
		for (vector<Directions>::iterator symmetryDirections2 =
			comparisonDirections.begin(); symmetryDirections2
			!= comparisonDirections.end(); symmetryDirections2++) {
				//cout << " comparing symmetryDirections " << endl;
				//cout << " symmetryDirections2 size: " << symmetryDirections2->size() << " with symmetryDirections1 size: " << symmetryDirections1->size() << endl;
				if ( (*symmetryDirections2 == *symmetryDirections1) ) {
					alreadyPresentInteraction = true;
				};
		};
		if (!alreadyPresentInteraction){
				//cout << " pushing back direction " << endl;
				uniqueSymmetryDirections.push_back(*symmetryDirections1);
		};

		comparisonDirections.clear();

	};
	//cout << "finished sorting out" << endl;

	// assigning sorted directions!
	symmetryDirections = uniqueSymmetryDirections;

	/* DEBUG
	cout << " -------- complete symmetryDirections after checking doubles----- " << endl;

	for (vector<Directions>::iterator symmetryVec1 = symmetryDirections.begin(); symmetryVec1
				!= symmetryDirections.end(); symmetryVec1++) {
		cout << "new interaction" << endl;
		for (Directions::iterator direction = symmetryVec1->begin(); direction != symmetryVec1->end(); direction++){
			cout << " - vector (" << direction->x << "/" << direction->y << ")" << endl;
		}
	}

	cout << " -------- complete symmetryDirections after doubles checking end ------------------------ " << endl << endl;
	*/

	return;
}


void Interactions::symmetrizeInteractions(Interactions& interactions) {
	//cout << "#############     calling symmetrizer  #################" << endl;
	for (Interactions::iterator interaction = interactions.begin(); interaction != interactions.end(); interaction++){
		//cout << "working on interaction " << interaction->name << endl;
		interaction->generateSymmetryEquivalentVecC2v(interaction->directions, interaction->symmetryDirections);
	};

	return;
}

void Interactions::joinSymmetryEquivalent(Interactions& interactions) {
	/* joining the equivalent representations of multi body interactions into one interaction object
	 * we are doing this, by going through interactions comparing the name
	 * if it is the same name, we are joining the direction and symmetryDirections object of the second one into the
	 * symmetryDirections object of the first one
	 * then the second direction gets deleted
	 *
	 */
	//cout << " -------- joining equivalent representations! --------" << endl;


	/*
	Interactions::iterator interaction = interactions.begin();
	Interactions joinedInteractions;
	while (interaction != interactions.end()) {
		if (joinedInteractions.empty()) {
			joinedInteractions.push_back(*interaction);
			interaction++;
			continue;
		} else { // checking for members of joinedInteractions...
			Interactions comparisonInteractions;
			comparisonInteractions.clear();
			comparisonInteractions.insert(comparisonInteractions.begin(),
					joinedInteractions.begin(), joinedInteractions.end());
			for (Interactions::iterator comparisonInteraction =
					comparisonInteractions.begin(); comparisonInteraction
					!= comparisonInteractions.end(); comparisonInteraction++) {
				cout << "interaction is: " << interaction->name << endl;
				cout << "comparing to interaction name: " << comparisonInteraction->name << endl;
				if (comparisonInteraction->name.compare(interaction->name) == 0 // exactly equal
						&& comparisonInteraction->directions // nothing to do
								== interaction->directions)
					continue;
				else if (comparisonInteraction->name.compare(interaction->name)
						== 0) { // equal representations - joining necessary
					Interactions::iterator joinedInteraction =
							joinedInteractions.begin();
					while (joinedInteraction->name.compare(
							comparisonInteraction->name) != 0)
						joinedInteraction++;
					cout << "joining equivalent representations" << endl;
					joinedInteraction->symmetryDirections.push_back(
							interaction->directions);
					cout << "pushed back original directions" << endl;
					joinedInteraction->symmetryDirections.insert(
							joinedInteraction->symmetryDirections.end(),
							interaction->symmetryDirections.begin(),
							interaction->symmetryDirections.end());
					cout << "pushed back symmetryDirections directions" << endl;
					//if ( interaction != interactions.end())
					//			interaction++;
					continue;
					}
				};
			}
		if (interaction != interactions.end()){
			joinedInteractions.push_back(*interaction);
			cout << "pushed back original interaction to joinedInteractions" << endl;
			interaction++;
		};
	};
	*/

	// new version of joining!

	Interactions joinedInteractions;
	if (joinedInteractions.empty())
			joinedInteractions.push_back(*(interactions.begin()));

	Interactions::iterator joinedInteraction = joinedInteractions.begin();
	Interactions::iterator joinedInteraction_end = joinedInteractions.end();
	int maxChecks = interactions.size();
	int nrOfChecks = 0;

	map<string, bool> pushedInteractions;

	pushedInteractions[interactions.begin()->name] = true;




	while (joinedInteraction != joinedInteraction_end && nrOfChecks <= maxChecks) {
		string workingInteractionName = joinedInteraction->name;
		for (Interactions::iterator interaction = interactions.begin(); interaction != interactions.end(); interaction++) {
			//cout << "joinedInteraction is: " << joinedInteraction->name << "\n";
			//cout << "Interaction is      : " << interaction->name << "\n";
			if (joinedInteraction->name.compare(interaction->name) == 0 && joinedInteraction->directions == interaction->directions)
			continue;
			if (joinedInteraction->name.compare(interaction->name) == 0) {
				joinedInteraction->symmetryDirections.push_back(
						interaction->directions);
				//cout << "pushed back original directions" << endl;
				joinedInteraction->symmetryDirections.insert(
						joinedInteraction->symmetryDirections.end(),
						interaction->symmetryDirections.begin(),
						interaction->symmetryDirections.end());
				//cout << "pushed back symmetryDirections directions" << endl;
				continue;
			}
			else { // checking if already pushed to joinedInteractions
				if ((pushedInteractions[interaction->name]))
					continue;
				else {
					//cout << " pushing back interaction : " << interaction->name << " to joinedInteractions \n";
					pushedInteractions[interaction->name] = true;
					joinedInteractions.push_back(*interaction); // push_back destroys all iterators
					joinedInteraction = joinedInteractions.begin(); // retrieveing valid iterators
					joinedInteraction_end = joinedInteractions.end();
					while (joinedInteraction->name.compare(workingInteractionName) != 0)
						joinedInteraction++; // now being at the original position again
					// joinedInteraction++; // new position for comparing
					continue;
				};
			};
		};
		joinedInteraction++;
		nrOfChecks++;
	};


	// interactions.assign(joinedInteractions.begin(), joinedInteractions.end());
	//cout << "interactions before consolidating" << endl;
	//cout << interactions << endl;

	//cout << "interactions after consolidating" << endl;
	//cout << joinedInteractions << endl;
	interactions = joinedInteractions;

	// new!
	//cout << "now checking once more for equivalent directions in symmetryDirections! \n";

	for (Interactions::iterator interaction = interactions.begin(); interaction
			!= interactions.end(); interaction++) {
		//cout << "working on interaction " << interaction->name << endl;
		Directions directions = interaction->directions;
		vector<Directions> symmetryDirections = interaction->symmetryDirections;

		vector<Directions> uniqueSymmetryDirections;

		for (vector<Directions>::iterator symmetryDirections1 =
				symmetryDirections.begin(); symmetryDirections1
				!= symmetryDirections.end(); symmetryDirections1++) {
			//cout << "comparing to original directions" << endl;
			if (*symmetryDirections1 == directions) {
				continue;
			};
			if (uniqueSymmetryDirections.empty())
				uniqueSymmetryDirections.push_back(*symmetryDirections1);

			vector<Directions> comparisonDirections;
			comparisonDirections.insert(comparisonDirections.end(),
					uniqueSymmetryDirections.begin(),
					uniqueSymmetryDirections.end());
			bool alreadyPresentInteraction = false;
			for (vector<Directions>::iterator symmetryDirections2 =
					comparisonDirections.begin(); symmetryDirections2
					!= comparisonDirections.end(); symmetryDirections2++) {
				//cout << " comparing symmetryDirections " << endl;
				//cout << " symmetryDirections2 size: "
				//		<< symmetryDirections2->size()
				//		<< " with symmetryDirections1 size: "
				//		<< symmetryDirections1->size() << endl;
				if ((*symmetryDirections2 == *symmetryDirections1)) {
					alreadyPresentInteraction = true;
				};
			};
			if (!alreadyPresentInteraction) {
				//cout << " pushing back direction " << endl;
				uniqueSymmetryDirections.push_back(*symmetryDirections1);
			};

			comparisonDirections.clear();

		};
		// assigning sorted directions!
		interaction->symmetryDirections = uniqueSymmetryDirections;
	};



}

void Interactions::generateCombinations(){
	/* this routine will use next_combination() from combination.hpp to generate all possible combinations of
	 * a given size out of the defined interations
	 * the purpose is to find the "best" set of interactions that is giving the lowest CV Score
	 * to describe the total energies of the input structures
	 *
	 * the original routine is from Ben Bear, Herve Bronnimann (2007)
	 * http://photon.poly.edu/~hbr/boost/combination.hpp
	 */

	const int r = 48; // size for the combination to be build = will start from 1 and will go to interactions.size()-1
	const int n = 48; // total number of elements in basis set = total number of interactions
	 std::vector<int> v_int(n);
	 for (int i = 0; i < n; ++i) { v_int[i] = i; } // generating starting vector for combinations

	 int N = 0;
	 do {
	     ++N;
	     std::cout << "[ " << v_int[0];
	         for (int j = 1; j < r; ++j) { std::cout << ", " << v_int[j]; }
	         std::cout << " ]" << std::endl;

	 } while (next_combination(v_int.begin(), v_int.begin() + r, v_int.end()));
	 std::cout << "Found " << N << " combinations of size " << r << " without repetitions"
	           << " from a set of " << n << " elements." << std::endl;
}

