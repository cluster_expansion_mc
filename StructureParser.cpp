#include <stdexcept>
#include "StringTokenizer.h"

#include <algorithm>
#include <iostream>
#include <stdexcept>

#include "StructureParser.h"

StructureParser::StructureParser(const char *ini_file_interactions,
		const char *ini_file_structure) :
	_ini_structure(false, true, false) {
	_structure = new Structure();
	cout << "parsing lattice" << endl;
	LatticeParser latticeParser(ini_file_structure);
	_structure->lattice = latticeParser.getLattice();
	cout << "printing lattice: " << endl;
	cout << *_structure->lattice;

	cout << "parsing interactions" << endl;
	InteractionsParser interactionsParser(ini_file_interactions);
	_structure->interactions = interactionsParser.getInteractions();

	//cout << "before symmetrizing" << _structure->interactions << endl;

	// Generate Symmetryequivalent Interactions
	_structure->interactions.symmetrizeInteractions(_structure->interactions);

	//cout << "interactions before joining double name occurencies " << endl << _structure->interactions << endl;

	// join symmetry equivalent representations of multi body interactions as given in input file by same name
	_structure->interactions.joinSymmetryEquivalent(_structure->interactions);

	//cout << "interactions (before assessment):" << endl << _structure->interactions << endl;
	_structure->lattice->assessInteractions(_structure->interactions);
	cout << "interactions (after assessment):" << endl << _structure->interactions;

	// Parse Structure Energetics
	SI_Error rc = _ini_structure.LoadFile(ini_file_structure);
	if (rc < 0)
		throw runtime_error("Failed loading structure ini file: "
				+ CSimpleIni::str_SI_Error(rc));

	double energy_value = 0.0;
	CSimpleIni::TNamesDepend energies;
	_ini_structure.GetAllKeys("OnSiteEnergies", energies);
	for (CSimpleIni::TNamesDepend::const_iterator i = energies.begin(); i
			!= energies.end(); ++i) {
		CSimpleIni::TNamesDepend values;
		_ini_structure.GetAllValues("OnSiteEnergies", i->pItem, values);
		for (CSimpleIni::TNamesDepend::const_iterator k = values.begin(); k
				!= values.end(); ++k) {
			string species = "";
			StringTokenizer strtok = StringTokenizer(k->pItem, ",");
			int cnt = strtok.countTokens();
			if (cnt == 0)
				throw runtime_error("Empty \"" + string(k->pItem) + "\" key.");
			for (int i = 0; i < cnt; i++) {
				string tmp = strtok.nextToken();
				if (sscanf(tmp.c_str(), "%lf", &energy_value) != 1) {
					// Treat last tuple as Occupation specifier
					if (i == cnt - 1)
						species = tmp;
					else
						throw runtime_error(
								"On Site Energy definition is wrong in: "
										+ string(tmp));
				}
			}
		}
		// FIXME : checking for different adsorbate species, defined by name, FIX: onSite energy is given as sum of all species here!
		_structure->onSiteEnergy = energy_value;

	};
	CSimpleIni::TNamesDepend dft_energies;
	if (!(_ini_structure.GetAllValues("DFT_Energy", "E0", dft_energies)))
		_structure->newStructure = true;
	else {
		double dft_energy = 0.0;
		for (CSimpleIni::TNamesDepend::const_iterator l = dft_energies.begin(); l
				!= dft_energies.end(); ++l) {
			if (sscanf(l->pItem, "%lf", &dft_energy) != 1)
				throw runtime_error("DFT Energy definition is wrong in: "
						+ string(l->pItem));
			_structure->dftEnergy = dft_energy;
		};
	};
}

Structure *StructureParser::getStructure() {
	// FIXME: auto_ptr?
	return _structure;
}

