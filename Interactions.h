#ifndef _INTERACTIONS_H
#define _INTERACTIONS_H

#include <string>
#include <vector>

using namespace std;

#include "Direction.h"
#include "Combination.hpp"

struct Interaction {
	string name;
	Directions directions;
	vector<Directions> symmetryDirections;
	double multiplicity;
	double energy;
	void generateSymmetryEquivalentVecC2v(Directions directions, vector<Directions>& symmetryDirections);
	friend bool operator==(Directions& firstDirections, Directions& secondDirections);
	Direction mirrorXZ(Directions::iterator direction);
	Direction mirrorYZ(Directions::iterator direction);
	// Direction rotate90degreeZ (Directions::iterator direction);
	vector<Directions> rotate90degreeZ (Directions& directions);
	Interaction(const string name = "", int multiplicity = 0, double energy =
			0.0) :
		name(name), multiplicity(multiplicity), energy(energy) {
	};
};






class Interactions: public vector<Interaction> {
public:
	static bool compareInteraction (const Interaction a, const Interaction b);
	friend ostream& operator<<(ostream& output, const Interactions&);
	void symmetrizeInteractions (Interactions& interactions);
	void joinSymmetryEquivalent (Interactions& interactions);
	void generateCombinations();

};

#endif
